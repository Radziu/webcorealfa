﻿using System;
using System.Collections.Generic;

namespace WebCoreAlfa.Models
{
    public partial class News
    {
        public int Id { get; set; }
        public string Tytul { get; set; }
        public string Post { get; set; }
        public DateTime? DataP { get; set; }
        public string ImageP { get; set; }
    }
}
