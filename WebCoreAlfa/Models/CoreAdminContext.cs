﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebCoreAlfa.Models
{
    public partial class CoreAdminContext : DbContext
    {
        public virtual DbSet<Main> Main { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=10.140.49.38;Initial Catalog=CoreAdmin;Persist Security Info=False;User ID=sa;Password=Chujwamwdupe1986!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Main>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Cpu).HasColumnName("cpu");

                entity.Property(e => e.LastInventory).HasColumnType("datetime");

                entity.Property(e => e.Loggeduser)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OsVersion)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
