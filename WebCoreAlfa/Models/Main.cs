﻿using System;
using System.Collections.Generic;

namespace WebCoreAlfa.Models
{
    public partial class Main
    {
        public int Id { get; set; }
        public string CompName { get; set; }
        public string Loggeduser { get; set; }
        public DateTime? LastInventory { get; set; }
        public string OsVersion { get; set; }
        public int? Ram { get; set; }
        public int? Cpu { get; set; }
        public int? HardNumber { get; set; }
    }
}
