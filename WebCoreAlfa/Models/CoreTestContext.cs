﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebCoreAlfa.Models
{
    public partial class CoreTestContext : DbContext
    {
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Telefony> Telefony { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=10.140.49.38;Initial Catalog=CoreTest;Persist Security Info=False;User ID=sa;Password=Chujwamwdupe1986!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<News>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DataP).HasColumnType("date");

                entity.Property(e => e.ImageP).IsUnicode(false);

                entity.Property(e => e.Post).IsUnicode(false);

                entity.Property(e => e.Tytul).IsUnicode(false);
            });

            modelBuilder.Entity<Telefony>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DataDo)
                    .HasColumnName("dataDo")
                    .HasColumnType("date");

                entity.Property(e => e.DataOd)
                    .HasColumnName("dataOd")
                    .HasColumnType("date");

                entity.Property(e => e.Faktura)
                    .HasColumnName("faktura")
                    .IsUnicode(false);

                entity.Property(e => e.Producent)
                    .HasColumnName("producent")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .HasColumnName("serial")
                    .IsUnicode(false);

                entity.Property(e => e.UserU)
                    .HasColumnName("userU")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Passwd)
                    .HasColumnName("passwd")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Passwd2)
                    .HasColumnName("passwd2")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserU)
                    .HasColumnName("userU")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
