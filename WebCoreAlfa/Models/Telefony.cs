﻿using System;
using System.Collections.Generic;

namespace WebCoreAlfa.Models
{
    public partial class Telefony
    {
        public int Id { get; set; }
        public string UserU { get; set; }
        public string Serial { get; set; }
        public string Faktura { get; set; }
        public DateTime? DataOd { get; set; }
        public DateTime? DataDo { get; set; }
        public string Producent { get; set; }
    }
}
