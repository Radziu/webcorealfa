﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebCoreAlfa.Models;

namespace WebCoreAlfa.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult Dashboard()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Glowna()
        {
            using (WebCoreAlfa.Models.CoreAdminContext con = new CoreAdminContext())
            {
                var q = (from s in con.Main select s).ToList();
                return View(q);
            }
                
        }
        public IActionResult Sprzet()
        {
            return View();
        }
    }
}
